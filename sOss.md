sOss
----

The __sO__bus __s__ervice __s__ubsystem.

Within sObus, the protocol id `1` is used.

# Messages

A message is built by converting the key/value pairs to a querystring.

Key definitions follow this syntax: _key_: _type_, _description_

Keys in _italics_ are optional, __bold__ keys are identifying

The message type is sent in every message in the `method` key. This key must be one of the following values: {request, respone, event, announcement}.

## Request
* __id__: string, the target service
* __reqId__: int, a request id; it is recommended to use the crc16 of the value
* _value_: var, the value of this request; a missing value field implies a read request

## Response
* __id__: string, the sending service
* __reqId__: the referred request
* _value_: var, the (possibly new) current value
* success: bool, states if the request was successful

## Event
* __id__: string, the service emitting the event
* _value_: var, an event value if applicable

## Announcement
* __id__: string, a unique idetifier of the service
* __grade__: string, either `event` or `service`
* group: string, a human readable title for this service group (used in UI)
* desc: string, a human readable title for this service (used in UI)
* type: string, the type this service expects value to be of; see `types`
* _min_: var, a minimum value
* _max_: var, a maximum value
* _step_: var, a step value
* _divider_: int, a divider to apply to values before sending, may be used to send fixed point floats
* access: string, `r`, `w` or `rw` declaring if this service may be read, written to, or both; optional for events

# Types
To allow smaller processors to read messages, the following types have been defined:

### int
a 32bit singed integer, fixed point numbers can be tranferred employing the `divider` field

### bool
a boolean value serialized to "0" or "1"

### string
a utf-8 string, note that not all devices may be able to process strings of arbitrary length

### color
a HTML/CSS style color definition, excluding color names
